#!/usr/bin/env python
# coding: utf-8

# In[1]:


from elasticsearch import Elasticsearch, helpers
import json, requests, re, os,csv, tldextract, datetime
from datetime import datetime
elastic_client = Elasticsearch()


# In[2]:


def init():
    
    ssl = []
    x509 = []
    dic = dict()
    dicc = dict()
    id = []
    fuids = []
    fuid = []
    
    query = {
    "size": 999999,
    "_source": ["@timestamp", "zeek.session_id", "network.bytes", "zeek.ssl.version", "network.transport",
                 "destination.geo.country_iso_code", "source.geo.country_iso_code", "source.geo.city_name", "destination.geo.city_name",
                 "zeek.x509.certificate.valid.from", "zeek.x509.certificate.valid.until", 
                 "zeek.x509.certificate.key.length", "zeek.ssl.cipher", "zeek.ssl.server.name",
                 "zeek.ssl.server.cert_chain_fuids", 'source.ip', 'destination.ip', 'source.port', 'destination.port',
                 "source.geo.location", "destination.geo.location", 'zeek.x509.certificate.issuer.organization',
                 "zeek.x509.certificate.issuer.country"
               ],
    "query": {
        "match_all": {}
    }
    }
    
    hello = elastic_client.search(index="demo-*", body=query, request_timeout=30)
    
    with open ('h.json', 'w') as n:
        json.dump(hello, n)
        n.close()
        
    with open ('h.json', 'r') as k:
        output = json.load(k)
        for x in output['hits']['hits']:
            if ((x.get('_source').get('zeek')) is not None):
                if ((x.get('_source').get('zeek').get('ssl')) is not None):
                    dic.update({'id' : x['_source']['zeek']['session_id']})
                    dic.update({'timestamp' : x['_source']['@timestamp']})
                    if ((x.get('_source').get('source')) is not None):
                        dic.update({'src_ip' : x.get('_source').get('source').get('ip')})
                        dic.update({'src_port' : x.get('_source').get('source').get('port')})
                    else:
                        dic.update({'src_ip' : 'None'})
                        dic.update({'src_port' : 'None'})
                    if ((x.get('_source').get('destination')) is not None):
                        dic.update({'dst_ip' : x.get('_source').get('destination').get('ip')})
                        dic.update({'dst_port' : x.get('_source').get('destination').get('port')})
                    else:
                        dic.update({'dst_ip' : 'None'})
                        dic.update({'dst_port' : 'None'})
                    if ((x.get('_source').get('source')) is not None):
                        if ((x.get('_source').get('source').get('geo')) is not None):
                            dic.update({'src_country' : x.get('_source').get('source').get('geo').get('country_iso_code')})
                            if ((x.get('_source').get('source').get('geo').get('city_name')) is not None):
                                dic.update({'src_city' : x.get('_source').get('source').get('geo').get('city_name')})
                            else:
                                dic.update({'src_city' : 'None'})
                            if ((x.get('_source').get('source').get('geo').get('location')) is not None):
                                dic.update({'src_geo' : x.get('_source').get('source').get('geo').get('location')})
                        else:
                            dic.update({'src_country' : 'None'})
                            dic.update({'src_city' : 'None'})
                    if ((x.get('_source').get('destination')) is not None):
                        if ((x.get('_source').get('destination').get('geo')) is not None):
                            dic.update({'dst_country' : x.get('_source').get('destination').get('geo').get('country_iso_code')})
                            if ((x.get('_source').get('destination').get('geo').get('city_name')) is not None):
                                dic.update({'dst_city' : x.get('_source').get('destination').get('geo').get('city_name')})
                            else:
                                dic.update({'dst_city' : 'None'})
                            if ((x.get('_source').get('destination').get('geo').get('location')) is not None):
                                dic.update({'dst_geo' : x.get('_source').get('destination').get('geo').get('location')})
                        else:
                            dic.update({'dst_country' : 'None'})
                            dic.update({'dst_city' : 'None'})
                    if ((x.get('_source').get('network')) is not None):
                        if ((x.get('_source').get('network').get('bytes')) is not None):
                            dic.update({'bytes' : x.get('_source').get('network').get('bytes')})
                        else:
                            dic.update({'bytes' : 0 })
                        if ((x.get('_source').get('network').get('transport')) is not None):
                            dic.update({'protocol' : x.get('_source').get('network').get('transport')})
                        else:
                            dic.update({'protocol' : 'None' })
                    else:
                        dic.update({'bytes' : 0 })
                    if ((x.get('_source').get('zeek').get('ssl').get('version')) is not None):
                        dic.update({'version' : x.get('_source').get('zeek').get('ssl').get('version')})
                    else:
                        dic.update({'version' : 'None'})
                    if ((x.get('_source').get('zeek').get('ssl').get('cipher')) is not None):
                        dic.update({'cipher' : x.get('_source').get('zeek').get('ssl').get('cipher')})
                    else:
                        dic.update({'cipher' : 'None'})
                    if ((x.get('_source').get('zeek').get('ssl').get('server')) is not None):
                        if ((x.get('_source').get('zeek').get('ssl').get('server').get('name')) is not None):
                            dic.update({'sni' : x.get('_source').get('zeek').get('ssl').get('server').get('name')})
                        else:
                            dic.update({'sni' : 'None'})
                        if ((x.get('_source').get('zeek').get('ssl').get('server').get('cert_chain_fuids')) is not None):
                            dic.update({'fuids' : x.get('_source').get('zeek').get('ssl').get('server').get('cert_chain_fuids')})
                        else:
                            dic.update({'fuids' : []})
                            
                    ssl.append(dic.copy())
                    
                if ((x.get('_source').get('zeek').get('x509')) is not None):
                    dicc.update({'id' : x['_source']['zeek']['session_id']})
                    dicc.update({'timestamp' : x['_source']['@timestamp']})
                    if ((x.get('_source').get('zeek').get('x509').get('certificate')) is not None):
                        dicc.update({'notbefore' : x.get('_source').get('zeek').get('x509').get('certificate').get('valid').get('from')})
                        dicc.update({'notafter' : x.get('_source').get('zeek').get('x509').get('certificate').get('valid').get('until')})
                        dicc.update({'key' : x.get('_source').get('zeek').get('x509').get('certificate').get('key').get('length')})
                        if ((x.get('_source').get('zeek').get('x509').get('certificate').get('issuer')) is not None):
                            if ((x.get('_source').get('zeek').get('x509').get('certificate').get('issuer').get('organization')) is not None):
                                dicc.update({'issuer_name' : x.get('_source').get('zeek').get('x509').get('certificate').get('issuer').get('organization')})
                            else:
                                dicc.update({'issuer_name' : 'None'})
                            if ((x.get('_source').get('zeek').get('x509').get('certificate').get('issuer').get('country')) is not None):
                                dicc.update({'issuer_country' : x.get('_source').get('zeek').get('x509').get('certificate').get('issuer').get('country')})
                            else:
                                dicc.update({'issuer_country' : 'None'}) 
                        else:
                            dicc.update({'issuer_name' : 'None'})
                            dicc.update({'issuer_country' : 'None'}) 
                    
                    x509.append(dicc.copy())
                    
    with open('ssl.json', 'w') as sslf:
        json.dump(ssl, sslf, indent = 2)
    sslf.close()
    
    with open('x509.json', 'w') as x509f:
        json.dump(x509, x509f, indent = 2)
    x509f.close()


# In[3]:


def join():
    
    ssl = []
    x509 = []
    dic = dict()
    final = []
    check = False
    
    with open('ssl.json', 'r') as sslf:
        ssl = json.load(sslf)
    sslf.close()
    
    with open('x509.json', 'r') as x509f:
        x509 = json.load(x509f)
    x509f.close()
    
    for x in x509:
        for z in ssl: 
            if (x['id'] in z['fuids']):
                check = True
                dic.update({'session_id' : z['id']})
                dic.update({'timestamp' : z['timestamp']})
                dic.update({'src_ip' : z['src_ip']})
                dic.update({'src_port' : z['src_port']})
                dic.update({'src_country' : z['src_country']})
                dic.update({'src_city' : z['src_city']})
                if (z.get('src_geo') is not None):
                    dic.update({'src_geo' : z['src_geo']})
                dic.update({'dst_ip' : z['dst_ip']})
                dic.update({'dst_port' : z['dst_port']})
                dic.update({'dst_country' : z['dst_country']})
                dic.update({'dst_city' : z['dst_city']})
                if (z.get('dst_geo') is not None):
                    dic.update({'dst_geo' : z['dst_geo']})
                dic.update({'tls_version' : z['version']})
                dic.update({'cipher_suite' : z['cipher']})
                dic.update({'sni' : z['sni']})
                dic.update({'protocol' : z['protocol']})
                dic.update({'bytes' : z['bytes']})
                dic.update({'cert_chain' : z['fuids']})
                dic.update({'cert_notbefore' : x['notbefore']})
                dic.update({'cert_notafter' : x['notafter']})
                dic.update({'cert_key_length' : x['key']})
                dic.update({'cert_issuer_org' : x['issuer_name']})
                dic.update({'cert_issuer_country' : x['issuer_country']})
                dic.update({'severity' : 6})
                dic.update({'no_sni' : 'no'})
                dic.update({'self_signed' : 'no'})
                dic.update({'not_1m' : 'no'})
                dic.update({'suspect_country' : 'no'})
                dic.update({'bad_cert' : 'no'})
                dic.update({'first_use' : 'no'})
                dic.update({'weak_specs' : 'no'})
                dic.update({'big_traffic' : 'no'})
                final.append(dic.copy())
                
    with open('final.json', 'w') as finalf:
        json.dump(final, finalf, indent = 2)
    finalf.close()
    
    os.remove("h.json")
    os.remove('ssl.json')
    os.remove('x509.json')


# In[4]:


def first():
    
    data = []
    dicc = dict()
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    for item in data:
        if (item['sni'] == 'None'):
            item['no_sni'] = 'yes'
            item['severity'] = 2
            
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[5]:


def second():
    
    data = []
    onem= []
    trusted = []
    dicc = dict()
    i = 0
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    with open('alexa.csv') as domains:
        for row in csv.reader(domains):
            onem.append(row[1])
            i += 1
            if(i >= 99):
                break
    domains.close()
    
    for x in data:
        for z in onem:
            if (x['sni'].endswith(z)):
                trusted.append(x['sni'])
                
    for item in data:
        if (item['sni'] not in trusted):
            if (len(item['cert_chain']) < 2):
                item['self_signed'] = 'yes'
                if (item['severity'] == 2):
                    item['severity'] = 1
                if (item['severity'] == 6):
                    item['severity'] = 2
            
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[6]:


def third():
    
    data = []
    onem = []
    alexa = []
    dicc = dict()
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    with open('umbrella.csv') as domains:
        for row in csv.reader(domains):
            onem.append(row[1])
    domains.close()
    
    with open('alexa.csv') as alexaf:
        for row in csv.reader(alexaf):
            alexa.append(row[1])
    alexaf.close()
    
    for x in data:
        e = tldextract.extract(x['sni'])
        if ((x['sni'] != "None") and (x['sni'] not in onem) and (e.registered_domain not in alexa)):
            x['not_1m'] = 'yes'
            if (x['severity'] == 2):
                x['severity'] = 1
            if (x['severity'] == 6):
                x['severity'] = 3
                    
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[7]:


def fourth():
    
    data = []
    dicc = dict()
    iso = ['CN', 'RU', 'IR', 'IL', 'KP']
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    for item in data:
        if (item['severity'] != 1):
            if ((item['src_country'] != "None") or (item['dst_country'] != "None")):
                if ((item['src_country'] in iso) or (item['dst_country'] in iso)):
                    item['suspect_country'] = 'yes'
                    if (item['severity'] == 2):
                        item['severity'] = 1
                    if (item['severity'] == 3):
                        item['severity'] = 2
                    if (item['severity'] == 6):
                        item['severity'] = 5
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[8]:


def fifth():
    
    data = []
    onem= []
    trusted = []
    window = 0
    i = 0
    dicc = dict()
    index = 6
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    with open('alexa.csv') as domains:
        for row in csv.reader(domains):
            onem.append(row[1])
            i += 1
            if(i >= 99):
                break
    domains.close()
    
    for x in data:
        for z in onem:
            if (x['sni'].endswith(z)):
                trusted.append(x['sni'])
    
    for item in data:
        d1 = datetime.strptime(item['cert_notbefore'], '%Y-%m-%dT%H:%M:%S.%fZ')
        d2 = datetime.strptime(item['cert_notafter'], '%Y-%m-%dT%H:%M:%S.%fZ')
        window =  abs((d2 - d1).days)
        now = datetime.now()
        validity = d2 > now
        key = not (item['cert_key_length'] < 2048 or item['cert_key_length'] > 4096)
        first = datetime.strptime(item['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
        start = (abs(first - d1).days)
            
        if ((item['sni'] not in trusted) and ((window < 90) or (validity == False) or (key == False))):
            item['bad_cert'] = 'yes'
            if (item['severity'] == 2):
                item['severity'] = 1
            if (item['severity'] == 3):
                item['severity'] = 2
            if (item['severity'] == 5):
                item['severity'] = 4
            if (item['severity'] == 6):
                item['severity'] = 4
        elif ((item['sni'] not in trusted) and (start < 7)):
            item['first_use'] = 'yes'
            if (item['severity'] == 2):
                item['severity'] = 1
            if (item['severity'] == 3):
                item['severity'] = 2
            if (item['severity'] == 4):
                item['severity'] = 3
            if (item['severity'] == 6):
                item['severity'] = 4
                
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[9]:


def sixth():
    
    data = []
    onem= []
    trusted = []
    i = 0
    dicc = dict()
    versions = ['TLSv12', 'TLSv13', 'unknown-64282']
    pref_cs = ['TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384', 'TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384',
               'TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305',  'TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305',
               'TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256', 'TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256',
               'TLS_ECDHE_ECDSA_WITH_AES_256_SHA384',     'TLS_ECDHE_RSA_WITH_AES_256_SHA384',
               'TLS_ECDHE_ECDSA_WITH_AES_128_SHA256',     'TLS_ECDHE_RSA_WITH_AES_128_SHA256']
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    with open('alexa.csv') as domains:
        for row in csv.reader(domains):
            onem.append(row[1])
            i += 1
            if(i >= 99):
                break
    domains.close()
    
    for x in data:
        for z in onem:
            if (x['sni'].endswith(z)):
                trusted.append(x['sni'])
                
    for item in data:
        version = item['tls_version']
        cs = item['cipher_suite']
        if ((item['sni'] not in trusted) and ((cs not in pref_cs) or (version not in versions))):
            item['weak_specs'] = 'yes'
            if (item['severity'] == 2):
                item['severity'] = 1
            if (item['severity'] == 3):
                item['severity'] = 2
            if (item['severity'] == 4):
                item['severity'] = 3
            if (item['severity'] == 5):
                item['severity'] = 4
            if (item['severity'] == 6):
                item['severity'] = 4
    
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[10]:


def seventh():
    
    data = []
    onem= []
    trusted = []
    dicc = dict()
    i = 0
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    with open('alexa.csv') as domains:
        for row in csv.reader(domains):
            onem.append(row[1])
            i += 1
            if(i >= 99):
                break
    domains.close()
    
    for x in data:
        for z in onem:
            if (x['sni'].endswith(z)):
                trusted.append(x['sni'])
                
    for item in data:
        bytes = item['bytes']
        if ((item['sni'] not in trusted) and (bytes > 104857600)):         #100 MB
            item['big_traffic'] = 'yes'
            if (item['severity'] == 2):
                item['severity'] = 1
            if (item['severity'] == 3):
                item['severity'] = 2
            if (item['severity'] == 4):
                item['severity'] = 3
            if (item['severity'] == 5):
                item['severity'] = 4
            if (item['severity'] == 6):
                item['severity'] = 5
                
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()


# In[11]:


def labels():
    
    data = []
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    for item in data:
        if (item['severity'] == 1):
            item['severity'] = "Critical risk"
        if (item['severity'] == 2):
            item['severity'] = "High risk"
        if (item['severity'] == 3):
            item['severity'] = "Suspicious"
        if (item['severity'] == 4):
            item['severity'] = "Low risk"
        if (item['severity'] == 5):
            item['severity'] = "Notice"
        if (item['severity'] == 6):
            item['severity'] = "Normal"
        
    with open('final.json', 'w') as finalf:
        json.dump(data, finalf, indent = 2)
    finalf.close()  


# In[12]:


def es():
    
    data = []
    
    if (not elastic_client.indices.exists('localhost:9200/invest')):
        elastic_client.indices.create(index='invest', ignore=400, request_timeout = 30)
    
    with open('final.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    elastic_client.indices.put_mapping(
    index="invest",
    doc_type="log",
    include_type_name=True,
    body={
        "properties": {  
            "timestamp": {"type":"date_nanos"},
            "cert_notbefore": {"type":"date_nanos"},
            "cert_notafter": {"type":"date_nanos"},
            "src_ip": {"type": "ip"},
            "dst_ip": {"type": "ip"},
            "src_port": {"type": "long"},
            "dst_port": {"type": "long"},
            "bytes": {"type": "long"},
            "cert_key_length": {"type": "long"},
            "src_geo": {"type": "geo_point"},
            "dst_geo": {"type": "geo_point"}
        }
    }
    )
    
    helpers.bulk(elastic_client, data, index='invest', doc_type='log', refresh=True, request_timeout = 30)
    
    params = (('preserve_existing', 'true'),)
    dataa = '{"index.max_result_window" : "999999"}'
    headers = {'Content-type': 'application/json',}
    requests.put('http://localhost:9200/invest*/_settings', headers=headers, params=params, data=dataa)
    
    os.remove("final.json")
    
    print("Done ... Exiting")


# In[13]:


init()
join()
first()
second()
third()
fourth()
fifth()
sixth()
seventh()
labels()
es()

