#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, ntpath, subprocess, time, json, requests, re, sys, signal, requests, base64, socket, elasticsearch, shutil
from os.path import expanduser
from elasticsearch import Elasticsearch, helpers
from datetime import datetime


# In[11]:


home = expanduser("~")+"/pcaps/"
filename = ""
count = 0
fpid = 0
print("Input directory: " + home)
elastic_client = Elasticsearch()
real_path = os.path.abspath('')
print(real_path)


# In[9]:


def ids():
    
    dic = dict()
    print(os.getcwd())
    mgmnt = []
    uniques = []
    
    query = {
    "size": 999,
    "_source": ['unique'],
    "query": {
        "match_all": {}
    }
    }
    try:
        hello = elastic_client.search(index="logmgmt", body=query, request_timeout=30)
    
        with open (real_path + '/pcaps.json', 'w') as n:
            json.dump(hello, n, indent = 2)
            n.close()
        
        with open (real_path + '/pcaps.json', 'r') as k:
            output = json.load(k)
            for x in output['hits']['hits']:
                uniques.append(x.get('_source').get('unique'))
        k.close()
        os.remove(real_path + '/pcaps.json')
    except elasticsearch.ElasticsearchException as es1:
        print('index')
    
    if not os.path.exists('/var/log/suricata'):
        os.makedirs('/var/log/suricata')
    if not os.path.exists('/var/log/bro'):
        os.makedirs('/var/log/bro')
    if not os.path.exists('/var/log/bro/current'):
        os.makedirs('/var/log/bro/current')
        
    for f in os.listdir(home):
        filename = ntpath.basename(f)
        if ((filename.endswith('.pcapng')) or (filename.endswith('.pcap'))):
            
            creation1 = (datetime.fromtimestamp(os.path.getctime(home + filename))).strftime("%Y-%m-%d %H:%M:%S")
            hostname = socket.gethostname()
            unique1 = str(filename) + str(creation1) + hostname
            unique2 = base64.b64encode(unique1.encode("utf-8"))
            unique3 = str(unique2, "utf-8")
            
            if (unique3 not in uniques):
                global count
                count+= 1
            
                size1 = os.popen('du -h -b ' + home + filename).read()
                size2 = size1.index('\t')
                size3 = size1[0:size2]
            
                creation1 = (datetime.fromtimestamp(os.path.getctime(home + filename))).strftime("%Y-%m-%d %H:%M:%S")
            
                now = (datetime.now()).strftime('%Y-%m-%d %H:%M:%S')
            
                count1 = os.popen('capinfos -Trc ' + home + filename).read()
                count2 = count1.index('\t')
                count3 = (count1[count2+1:len(count1)]).rstrip()
            
                hostname = socket.gethostname()
                unique1 = str(filename) + str(creation1) + hostname
                unique2 = base64.b64encode(unique1.encode("utf-8"))
                unique3 = str(unique2, "utf-8")
            
                dic.update({'name' : filename})
                dic.update({'creation_date' : creation1})
                dic.update({'processing_date' : now})
                dic.update({'size' : size3})
                dic.update({'packets' : count3})
                dic.update({'unique' : unique3})
            
                mgmnt.append(dic.copy())
            
                print('Suricata started')
                os.system('suricata -c /etc/suricata/suricata.yaml -k none -l /var/log/suricata/ -r ' + home + filename)
                os.chdir('/var/log/bro/current')
                print('Zeek started')
                os.system('/nsm/bro/bin/zeek -C -r ' + home + filename)
            
    os.chdir(real_path)        
    with open('mgmnt.json', 'w') as filemgmnt:
        json.dump(mgmnt, filemgmnt, indent = 2)
    filemgmnt.close()
            
    return (count)


# In[6]:


def filebeat():
    
    global fpid

    print('Filebeat started')
    filebeat = subprocess.Popen('filebeat', stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
    time.sleep(5)
    get = os.popen("pidof filebeat")
    output = get.read()
    fpid = int(output)

    return (fpid)


# In[4]:


def stop():
    
    values = []

    while True:
        output = elastic_client.count(index="demo-*", request_timeout = 30)
        response = json.dumps(output)
        data = json.loads(response)
        aggs = data['count']
        values.append(aggs)
        
        cat = os.popen('cat /var/log/suricata/eve.json | wc -l').read()
        cat2 = int(cat)
        
        time.sleep(30)
        
        if ((aggs in values) or cat2 == 0):
            print("Stopping Filebeat")
            os.kill(fpid, signal.SIGTERM)
            print('Changing Index Max Result Window')
            params = (('preserve_existing', 'true'),)
            data = '{"index.max_result_window" : "999999"}'
            headers = {'Content-type': 'application/json',}
            response = requests.put('http://localhost:9200/demo-*/_settings', headers=headers, params=params, data=data)
            print("Launching classification ...")
            break


# In[7]:


def mgmt():
    
    data = []
    
    if (not elastic_client.indices.exists('localhost:9200/logmgmt')):
        elastic_client.indices.create(index='logmgmt', ignore=400, request_timeout = 30)
    
    with open('mgmnt.json', 'r') as file:
        data = json.load(file)
    file.close()
    
    elastic_client.indices.put_mapping(
    index="logmgmt",
    doc_type="log",
    include_type_name=True,
    body={
        "properties": {  
            "creation_date": {"type":"date", "format": "yyyy-MM-dd HH:mm:ss"},
            "processing_date": {"type":"date", "format": "yyyy-MM-dd HH:mm:ss"},
            "size": {"type": "long"},
            "packets": {"type": "long"}
        }
    }
    )
    
    helpers.bulk(elastic_client, data, index='logmgmt', doc_type='log')
    os.remove(real_path + '/mgmnt.json')


# In[99]:


ids()
if (count > 0):
    mgmt()
    filebeat()
    stop()
    os.system('python3 process.py')
    os.remove('/var/lib/filebeat/registry/filebeat/data.json')
    shutil.rmtree('/var/log/suricata')
    shutil.rmtree('/var/log/bro/current') 